.PHONY: .build

PWD = $(shell pwd)
HOST_PORT_1 = 8081
HOST_PORT_2 = 8082
PORT_RANGE = 50000-50050

ec:
	echo $(PWD)

build:
	sudo docker build -t luciferreficul/proftpd:latest .

run:
	echo $(pwd)
	sudo docker run -d -p $(HOST_PORT_1):21 -p $(HOST_PORT_2):22 -p $(PORT_RANGE):$(PORT_RANGE) \
		-v $(PWD)/ftp:/home/proftpd \
		-v $(PWD)/config/proftpd/:/etc/proftpd/ \
		-v $(PWD)/config/proftpd/conf.d/:/etc/proftpd/conf.d/ \
		-v $(PWD)/config/proftpd.conf:/etc/proftpd.conf \
		luciferreficul/proftpd:latest
