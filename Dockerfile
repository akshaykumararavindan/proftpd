FROM registry.access.redhat.com/ubi8/ubi

RUN rpm --install --quiet https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm \
    && dnf update -y \
    && dnf clean all

RUN dnf install --assumeyes --quiet epel-release proftpd procps-ng ufw lsof perl

RUN useradd -m -p $(perl -e 'print crypt($ARGV[0], "password")' 'proftpd') proftpd

RUN mkdir /var/log/wtmp

RUN touch /var/log/wtmp

RUN chmod 666 /var/log/wtmp

EXPOSE 20 21 50000-50100

# ENTRYPOINT [ "/docker-entrypoint.sh" ]
CMD ["proftpd", "--nodaemon"]
